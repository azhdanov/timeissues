require 'rubygems'
require 'rexml/document'
require 'bitbucket_rest_api'
require 'reverse_markdown'

# Downgrade JIRA to Bitbucket issues
#
# Using bitbucket_rest_api and xml search results.
#
# Not possible to set created_on, reported_by and upload attachment
# One field for version, instead of two fields (Affected and Fixed) in JIRA
#
# See also https://confluence.atlassian.com/display/BITBUCKET/issues+Resource#issuesResource-POSTanewissue

# USAGE:
#
# gem install bitbucket_rest_api
# gem install reverse_markdown
# export basic_auth=email:password
# export DEBUG=true
#
# ruby jira-to-bb.rb

include REXML

USER = 'azhdanov'
REPO = 'jiratimesheet'

VERSIONS = %w(1.8 2.2.9 2.3.9 2.3.10)

def main
  xml = File.new('SearchRequest.xml')
  doc = REXML::Document.new xml

  bitbucket = BitBucket.new :user => USER, :repo => REPO,  :basic_auth => ENV["basic_auth"]

=begin CUSTOM EDITION
  versions = bitbucket.versions.all USER, REPO
  VERSIONS.reverse.each do |version|
    i = versions.index { |v| v.name == version}
    if (i.nil?)
      puts bitbucket.versions.create USER, REPO, version
    end
  end
=end

  doc.elements.each('rss/channel/item') do |item|
    next if item.elements['key'].text != 'TIME-317'
    issue = bitbucket.issues.create USER, REPO,
        'title' => item.elements['title'].text,
        'content' => markdown(item.elements['description']) + "\n\n" +
         'By ' + username(item.elements['reporter'].attributes['username']) +
          '/' + item.elements['reporter'].text +
          ' on ' + item.elements['created'].text,
        "responsible" => "azhdanov",
        'kind' => kind(item),
        'status' => status(item),
        #"milestone" => 1,
        'priority' => priority(item),
        'version' => version(item)
    next if (!item.elements['comments'])
    item.elements['comments'].elements.each do |comment|
      bitbucket.issues.comments.create USER, REPO, issue.local_id,
        :content => markdown(comment) + "\n\n" +
        'By ' + username(comment.attributes['author']) +
        ' on ' + comment.attributes['created']
    end
  end
end

=begin
 1 = Fixed
 2 = Won't Fix
 3 = Duplicate
 4 = Incomplete
 5 = Cannot Reproduce
 6 = Invalid
 7 = Support Request
 8 = Answered
10 = Timed out
11 = Obsolete
12 = Resolved Locally
13 = Awaiting Development
=end
def status item
  case item.elements['resolution'].attributes['id'].to_i
  when 1, 7, 8,12 # Fixed, Support Request, Answered, Resolved Locally
    'resolved'
  when 2
    'wontfix'
  when 3 
    'duplicate'
  when 4,5,6,10,11 # Incomplete, Cannot Reproduce, Invalid, Timed Out, Obsolete
    'invalid'
  when 13 # Awaiting Developmen
    'on hold'
  else
    'open'
  end
end


def priority item
  jira_priority_text = item.elements['priority'].text
  # uncapitalize
  jira_priority_text[0, 1].downcase + jira_priority_text[1..-1]
end

=begin
 1 = Bug
 2 = New Feature
 3 = Task
 4 = Improvement
 6 = Support Request
 7 = Third-party issue
 8 = Epic
 9 = Story
=end
def kind item
  case item.elements['type'].attributes['id'].to_i
  when 2, 4, 8, 9 # New Feature, Improvement, Epic, Story
    'enhancement'
  when 3 
    'task'
  else # Bug, Support Request, Thirt-party issue,
    'bug'
  end
end

def version item
  version = item.elements['fixVersion']
  if (!version.nil?)
    v = version.text
    if (v.start_with? '1.' and v != '1.9')
      '1.8'
    elsif (v == '1.9' || (v.start_with?('2.') and !v.start_with?('2.3')))
      '2.2.9'
    else # start_with 2.3
      '2.3.9'
    end
  end
end

# [TIME-212](https://studio.plugins.atlassian.com/browse/TIME-212 "Time Sheet gadget doesn't work over SSL (HTTPS)")
# 1.  TIME-212
# 2.  TIME
# 3.  212
# 4.  https://studio.plugins.atlassian.com/browse/TIME-212
# 5.  
# 6.  "Time Sheet gadget doesn't work over SSL (HTTPS)"
# 7.  Time Sheet gadget doesn't work over SSL (HTTPS)
#                 12     3            4         5     6 7
R = Regexp.new(/\[((\w+)-(\d+))\]\s*\(([^"\s]+)?(\s*?)("(.+?)")?\)/)

def markdown element
  text = ReverseMarkdown.parse element.text
  text.gsub(R, 'issue #\3 "[\1]\5\7"')
end

E = Regexp.new(/^(.+?)@.+$/)
def username username
  username.sub(E, '\1')
end

main
