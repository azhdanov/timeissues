require 'bitbucket_rest_api'

# Delete issues using bitbucket_rest_api 
#
# See also https://confluence.atlassian.com/display/BITBUCKET/issues+Resource#issuesResource-DELETEanissue

# USAGE:
#
# gem install bitbucket_rest_api
# export basic_auth=email:password
# export DEBUG=true
#
# ruby delete-issues.rb

USER = 'whyves'
REPO = 'rekall'

def main

  bitbucket = BitBucket.new :user => USER, :repo => REPO,  :basic_auth => ENV["basic_auth"]

  $issueNum = 13
  $end = 13 #763

  while $issueNum <= $end
    $issue = bitbucket.issues.delete USER, REPO, $issueNum
    $issueNum = $issueNum + 1
  end
end

main
